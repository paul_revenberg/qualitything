﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace carTests
{
    public class PolicyHolderTest
    {

        [Theory]
        [ClassData(typeof(LicenseAgeTestData))]
        public void LicenseAgeTest(int expected, string dateString)
        {
            PolicyHolder ph = new PolicyHolder(28, dateString, 6666, 0);

            Assert.Equal(expected, ph.LicenseAge);
        }

        [Theory]
        [InlineData(true, "2022-10-20")]
        [InlineData(true, "10/12/1922")]
        [InlineData(true, "12-12-2020")]
        [InlineData(false, "12122020")]
        [InlineData(false, "12-30-2020")]
        public void LicenseAgeFormatTest(bool shouldFail, string dateString)
        {
            var exception = Record.Exception(() =>
            {
                PolicyHolder ph = new PolicyHolder(28, dateString, 6666, 0);
            });
            if (shouldFail) Assert.Null(exception);
            else Assert.IsType<FormatException>(exception);

        }
    }
}
public class LicenseAgeTestData : IEnumerable<object[]>
{
    private readonly List<object[]> _data = new List<object[]>
    {
        new object[] {DateTime.Now.Year - 2022-1, "10-10-2022"},
        new object[] {0, DateTime.Now.ToString(new CultureInfo("nl-NL"))},
        new object[] {DateTime.Now.Year - 1990-1, "10-10-1990"},
    };

    public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
