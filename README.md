# Introductie
Dit is de CarSurance applicatie.
In deze README zal je lezen over de Unit tests die gemaakt en uitgevoerd zijn, ook vind je hier de gevonden fouten in de codes.

## De Testen

### PolicyHolderTest
#### Het doel
Het doel van de unit test voor PolicyHolder is om te testen of de Datums die berekend worden kloppen.
#### De data
De meegegeven data is:
1. Jaar-maand-dag; 4 maanden en 6 dagen in de toekomst, gescheiden door een -.
2. Dag/maand/jaar; 2 dagen 6 maanden en 99 jaar in het verleden, gescheiden door een /.
3. Dag-maand-jaar; 2 dagen 6 maanden en 1 jaar in het verleden, gescheiden door een -.
4. DagMaandJaar; 2 dagen 6 maanden en 1 jaar in het verleden, niet gescheiden.
5. Maand, dag, jaar; 
#### technieken die gebruikt zijn om op deze data te komen
- 1,2,3,4,5 testen of verschillende manieren van datum invoeren goed afgehandeld worden, of dat er een exception error komt.
### VehicleTest
#### Het doel
Het doel van de unit test voor Vehicle is om te testen of het bouwjaar die berekend worden kloppen.
#### De data
Als datum wordt meegegeven:
1. dit jaar
2. vorig jaar
3. volgend jaar
4. 10 jaar verleden
5. 10 jaar toekomst
6. int max
#### Technieken die gebruikt zijn om op deze data te komen
- 1, 2, 3 kijken hoe hij met 0 en lage waarden omgaat
- 3, 5 kijken hoe hij omgaat met negatieve waarden
- 2, 4 kijken hoe hij omgaat met normale waarden
- 6 kijkt hoe hij omgaat met hoge waarden
### PremiumCalculationTest
#### Het doel
Het doel van de unit test voor PremiumCalculations is om te testen of de berekeningen goed gaan op basis van de gegevens van de PolicyHolders, Vehicles en verzekering.
#### De data
-PolicyHolder standaard
	- Leeftijd 28 jaar
	- Rijbewijs 5 jaar oud
	- Postcode 6666 
	- no claim 0
-Vehicles standaard
	- 500 kw
	- 20.000 euro
	- leeftijd 15 jaar oud
-Verzekering
	- WA
#### Technieken
-PolicyHolder
1. 28 jaar is meer dan de maximale 22 jaar dat voor toeslag zorgt
2. Een rijbewijs van 5 jaar oud is meer dan de 4 jaar waardoor er toeslag komt
3. De postcode van 6666 is meer dan 4499 wat voor toeslag zorgt
4. Een no claim van 0 is minder dan de 6 jaar waardoor je geen korting krijgt
1, 2, 3, 4 zijn waarden waarmee alle toeslagen van toepassing zijn en er geen korting aanwezig is.
-Vehicle
De waarden in Vehicle standaard zijn willekeurige waarden, het maakt namelijk niks uit voor toeslagen en kortingen.
-Verzekering
1. WA zorgt niet voor extra toeslagen

### PremiumLicenseExtraTest
#### Het doel
Het doel van de unit test voor PremiumLicenseExtra is om te testen of er met minder van 5 jaar rijbewijs correcte toeslagen berekend worden.
#### De data
1. 4 jaar 364 dagen
2. 5 jaar
3. 6 jaar
#### Technieken
- 1, 2 zijn randwaarden voor de 5 jaar
- 3 is een waarde boven 5 jaar die niet voor toeslagen zou moeten zorgen
### PremiumHolderExtraTest
#### Het doel
Het doel van de unit test voor PremiumAgeSurCharge is om te testen of er met een leeftijd jonger dan 23 de correcte toeslagen berekend worden.
#### De data
1. 22 jaar
2. 23 jaar
3. 24 jaar
#### Technieken
- 1, 2 zijn randwaarden van 23 jaar
- 3 is hoger dan 23 jaar om te kijken of de toeslagen goed berekend worden
### PremiumNoClaimYearsDiscountTest
#### Het doel 
Het doel van de unit test voor PremiumNoClaimYearsDiscount is om te testen of er met 5 of meer jaar schadevrij de correcte korting berekend wordt.
#### De data
1. 5 jaar randvoorwaarden
2. 6
3. 12
4. 18
5. 19
#### Technieken
- 1, 2 zijn randwaarden voor 5 jaar
- 4, 5 zijn randwaarden voor de maximale korting van 65%
- 3 is een willekeurig jaar om te kijken of dit goed wordt afgehandeld
### PremiumPostalCodeSurChargeTest
#### Het doel
Het doel van de unit test voor PremiumPostalCodeSurCharge is om te testen of op basis van postcode de toeslagen correct berekend worden.
#### De data
1. 0999
2. 1000
3. 3600
4. 3599
5. 4499
6. 4500
7. 6666
#### Technieken
- 1, 2, 3, 4 zijn de randwaarden voor de postcodes van 10.. – 35..
- 3, 4, 5, 6 zijn de randwaarden voor de postcodes van 36.. – 44..
- 7 is een waarde die buiten de toeslagen zou moeten vallen, om te kijken of dit goed afgehandeld wordt
### PremiumInsuranceTypeSurchargeTest
#### Het doel
Het doel van de unit test voor PremiumInsuranceTypeSurCharge is om te testen of op basis van de verzekering die gekozen wordt de toeslagen correct berekend worden.
#### De data
1. WA
2. WA_Plus
3. AllRisk
#### Technieken
- 1, 2, 3 deze 3 opties worden allemaal 1 keer getest
### PremiumMonthlyPaymentSurchargeTest
#### Het doel
Het doel van de unit test voor PremiumMonthlyPaymentSurCharge is om te testen of de juiste prijzen getoond worden voor de gekozen periodes.
#### De data
1. Monthly payments
2. Yearly payments
#### Technieken
- 1 checkt of de maandelijkse betalingen goed afgehandeld worden
- 2 checkt of de jaarlijkse betalingen goed afgehandeld worden
### BasePremiumTest
#### Het doel
Het doel van de unit test voor BasePremium is om te testen of de juiste basePremium wordt berekend.
#### De data
- default vehicle
	1. 500 kw
	2. 20.000 euro
	3. leeftijd 15 jaar oud
#### Technieken
- 1, 2, 3 zijn willekeurige waarden. Bij deze waarden komen er geen toeslagen bij.
## Fouten
### Program.cs
Geen fouten aangepast.
### PremiumCalculation
-	Bij line 57 stond er < 4500 => premium * 1.02. Hierdoor werden alle postcodes onder de 4500 meegenomen i.p.v. tussen de 3600 en 4500. Dit is aangepast naar >= 3600 and < 4500 => premium * 1.02
-	Bij line 49 en 50 de 2 if statements aangepast. De berekening gaat keer 5, in de code stond dit niet correct, waardoor hij niet door de stryker test heen zou komen. Dit heb ik aangepast naar 
If (NoClaimPercentage > 70) {NoClaimPercentage = 65; } en 
If (NoClaimPercentage < -5) {NoClaimPercentage = 0; }
-	Bij line 51 stond /100, dit heb ik aangepast naar * 0.01.
-	Bij line 26 stond policyholder <= 5, dit heb ik aangepast naar < dan 5.
-	Bij line 69 heb ik de return aangepast door er (double) voor te plaatsen.
 
