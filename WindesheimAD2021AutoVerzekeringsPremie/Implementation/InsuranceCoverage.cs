﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("carTests")]
namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    enum InsuranceCoverage
    {
        WA,
        WA_PLUS,
        ALL_RISK
    }
}
