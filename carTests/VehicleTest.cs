﻿using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using System;
using System.Collections.Generic;
using System.Collections;

namespace carTests
{
    public class VehicleTest
    {
        [Theory]
        [ClassData(typeof(VehicleTestData))]
        public void VehicleAgeTest(long expected, int constructionYear)
        {
            var vehicle = new Vehicle(1, 1, constructionYear);
            Assert.Equal(expected, vehicle.Age);
        }
    }
}


public class VehicleTestData : IEnumerable<object[]>
{
    private readonly List<object[]> _data = new List<object[]>
    {
        new object[] {0, DateTime.Now.Year},
        new object[] {1,DateTime.Now.Year - 1},
        new object[] {0,DateTime.Now.Year - -1},
        new object[] {10,DateTime.Now.Year - 10},
        new object[] {0,DateTime.Now.Year - -10},
        new object[] {0,int.MaxValue},
    };

    public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}