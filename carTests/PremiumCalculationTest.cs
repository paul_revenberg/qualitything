﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace carTests
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void BasePremiumTest()
        {
            double basePremium = PremiumCalculation.CalculateBasePremium(NewVehicle());
            Assert.Equal(95, basePremium,2);
        }

        [Theory]
        [ClassData(typeof(LicenseTestData))]
        public void PremiumLicenseExtraTest(double expected, DateTime licenseDate) 
        {
            var ph = NewPolicyHolder(28, licenseDate);
            var calc = new PremiumCalculation(NewVehicle(), ph, InsuranceCoverage.WA);
            Assert.Equal(expected, Math.Round(calc.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(109.25, 22)]
        [InlineData(95, 23)]
        [InlineData(95, 24)]
        public void PremiumHolderAgeExtraTest(double expected, int age)
        {
            PolicyHolder ph = NewPolicyHolder(age);
            var calc = new PremiumCalculation(NewVehicle(), ph, InsuranceCoverage.WA);
            Assert.Equal(expected, calc.PremiumAmountPerYear, 2);
        }

        [Theory]
        [InlineData(95, InsuranceCoverage.WA)]
        [InlineData(114, InsuranceCoverage.WA_PLUS)]
        [InlineData(190, InsuranceCoverage.ALL_RISK)]
        internal void PremiumInsuranceTypeExtraTest(double expected, InsuranceCoverage type)
        {
            var calc = new PremiumCalculation(NewVehicle(), NewPolicyHolder(), type);
            Assert.Equal(expected, calc.PremiumAmountPerYear, 2);
        }

        [Theory]
        [InlineData(95, 5)]
        [InlineData(90.25, 6)]
        [InlineData(61.75, 12)]
        [InlineData(33.25, 18)]
        [InlineData(33.25, 19)]
        public void NoClaimYearsExtraTest(double expected, int unClaimedYears)
        {
            var ph = NewPolicyHolder(28, null, 6666, unClaimedYears);
            var calc = new PremiumCalculation(NewVehicle(), ph, InsuranceCoverage.WA);
            Assert.Equal(expected, calc.PremiumAmountPerYear, 2);
        }

        [Theory]
        [InlineData(95, 0999)]
        [InlineData(99.75, 1000)]
        [InlineData(99.75, 3599)]
        [InlineData(96.90, 3600)]
        [InlineData(96.90, 4499)]
        [InlineData(95, 4500)]
        public void PostalCodeExtraTest(double expected, int postalCode)
        {
            var ph = NewPolicyHolder(28, null, postalCode);
            var calc = new PremiumCalculation(NewVehicle(), ph, InsuranceCoverage.WA);
            Assert.Equal(expected, calc.PremiumAmountPerYear, 2);

        }

        [Theory]
        [InlineData(7.92, PaymentPeriod.MONTH)]
        [InlineData(92.62, PaymentPeriod.YEAR)]
        internal void TermPaymentDiscountTest(double expected, PaymentPeriod period)
        {
            var calc = new PremiumCalculation(NewVehicle(), NewPolicyHolder(), InsuranceCoverage.WA);
            Assert.Equal(expected, calc.PremiumPaymentAmount(period),2);
        }

        private static PolicyHolder NewPolicyHolder(int age = 28, DateTime? licenseDate = null, int postalCode = 6666, int noClaimYears = 0)
        {
            DateTime licenseStartDate = DateTime.Now.AddYears(-5); ;
            if (licenseDate != null) licenseStartDate = (DateTime)licenseDate;

            string licenseStartDateString = String.Format("{0}-{1}-{2}", licenseStartDate.Day, licenseStartDate.Month, licenseStartDate.Year);
            return new (age, licenseStartDateString, postalCode, noClaimYears);
        }

        //Default values have a base premium of 95
        private static Vehicle NewVehicle(int PowerInKw = 500, int ValueInEuros = 20000, int age =  15)
        {
            DateTime buildYear = DateTime.Now;
            buildYear = buildYear.AddYears(-age);
            return new Vehicle(PowerInKw, ValueInEuros, buildYear.Year);
        }
    }

}

public class LicenseTestData : IEnumerable<object[]>
{
    private readonly List<object[]> _data = new List<object[]>
    {
        new object[] {95, DateTime.Now.AddYears(-6)},
        new object[] {95, DateTime.Now.AddYears(-5)},
        new object[] {109.25, DateTime.Now.AddYears(-5).AddDays(1)},
    };

    public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}